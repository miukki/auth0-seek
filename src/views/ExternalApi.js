import React, { useState } from "react";
import { Button, Alert } from "reactstrap";
import Highlight from "../components/Highlight";
import { useAuth0, withAuthenticationRequired } from "@auth0/auth0-react";
import config from "../auth_config.json";
import Loading from "../components/Loading";

const { apiOrigin = "http://localhost:3001" } = config;

export const ExternalApiComponent = () => {
  const [state, setState] = useState({
    showResult: false,
    apiMessage: "",
    error: null,
  });

  const {
    getAccessTokenSilently,
    loginWithPopup,
    getAccessTokenWithPopup,
  } = useAuth0();

  const handleConsentCandidate = async () => {
    try {
      await getAccessTokenWithPopup({
        scope: `read:candidate`,
      });
      setState({
        ...state,
        error: null,
      });
    } catch (error) {
      setState({
        ...state,
        error: error.error,
      });
    }

    await callCandidate();
  };

  const handleConsentRecruiter = async () => {
    try {
      await getAccessTokenWithPopup({
        scope: `read:recruiter`,
      });
      setState({
        ...state,
        error: null,
      });
    } catch (error) {
      setState({
        ...state,
        error: error.error,
      });
    }

    await callRecruiter();
  };

  const handleLoginAgain = async () => {
    try {
      await loginWithPopup();
      setState({
        ...state,
        error: null,
      });
    } catch (error) {
      setState({
        ...state,
        error: error.error,
      });
    }

    await callCandidate();
  };

  const callRecruiter = async () => {
    try {
      const token = await getAccessTokenSilently({
        scope: `read:recruiter`,
      });

      const response = await fetch(`${apiOrigin}/api/recruiter`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      const responseData = await response.json();

      setState({
        ...state,
        showResult: true,
        apiMessage: responseData,
      });
    } catch (error) {
      setState({
        ...state,
        error: error.error,
      });
    }
  };

  const callCandidate = async () => {
    try {
      const token = await getAccessTokenSilently({
        scope: `read:candidate`,
      });

      const response = await fetch(`${apiOrigin}/api/candidate`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      const responseData = await response.json();

      setState({
        ...state,
        showResult: true,
        apiMessage: responseData,
      });
    } catch (error) {
      setState({
        ...state,
        error: error.error,
      });
    }
  };

  const handle = (e, fn) => {
    e.preventDefault();
    fn();
  };

  return (
    <>
      <div className="mb-5">
        {state.error === "consent_required" && (
          <Alert color="warning">
            You need consent to get access to users api
          </Alert>
        )}

        {state.error === "login_required" && (
          <Alert color="warning">
            You need to{" "}
            <a
              href="#/"
              class="alert-link"
              onClick={(e) => handle(e, handleLoginAgain)}
            >
              log in again
            </a>
          </Alert>
        )}

        <h1>External API</h1>
        <Button
          color="primary"
          className="mt-5"
          onClick={handleConsentRecruiter}
        >
          REQUEST RECRUITER API
        </Button>

        <Button
          color="primary"
          className="mt-5"
          onClick={handleConsentCandidate}
        >
          REQUEST CANDIDATE API
        </Button>

        <Button color="primary" className="mt-5" onClick={callRecruiter}>
          RECRUITER API
        </Button>

        <Button color="primary" className="mt-5" onClick={callCandidate}>
          CANDIDATE API
        </Button>
      </div>

      <div className="result-block-container">
        {state.showResult && (
          <div className="result-block" data-testid="api-result">
            <h6 className="muted">Result</h6>
            <Highlight>
              <span>{JSON.stringify(state.apiMessage, null, 2)}</span>
            </Highlight>
          </div>
        )}
      </div>
    </>
  );
};

export default withAuthenticationRequired(ExternalApiComponent, {
  onRedirecting: () => <Loading />,
});
